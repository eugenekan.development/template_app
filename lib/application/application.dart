import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kreios_app/bloc/auth/auth_bloc.dart';
import 'package:kreios_app/bloc/localization/localization_bloc.dart';
import 'package:kreios_app/feature/theme/bloc/theme_bloc.dart';
import 'package:kreios_app/injection/injection.dart';
import 'package:kreios_app/localization/k_localization.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:kreios_app/router/app_router.gr.dart';
import 'package:kreios_app/service/theme_service.dart';
import 'package:kreios_app/utils/dimensions/dimension_provider.dart';

class MyApp extends StatelessWidget {
  const MyApp({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => MultiBlocProvider(
        providers: [
          BlocProvider(create: (_) => getIt<AuthBloc>()),
          BlocProvider(create: (_) => getIt<ThemeBloc>()),
          BlocProvider(create: (_) => getIt<LocalizationBloc>()),
        ],
        child: BlocBuilder<ThemeBloc, ThemeState>(
          builder: (context, state) {
            const themeService = ThemeService();
            return BlocBuilder<LocalizationBloc, LocalizationState>(
              builder: (context, localizationState) => MaterialApp.router(
                title: 'Flutter Demo',
                routerDelegate: getIt<AppRouter>().delegate(),
                routeInformationParser: getIt<AppRouter>().defaultRouteParser(),
                theme: state.map(
                  initial: (_) => themeService.defaultTheme,
                  setTheme: (value) => value.themeData,
                ),
                locale: localizationState.locale,
                supportedLocales: KLocalization.supportedLocales,
                localizationsDelegates: const [
                  KLocalization.delegate,
                  GlobalMaterialLocalizations.delegate,
                  GlobalWidgetsLocalizations.delegate,
                ],
                builder: (context, child) => DimensionsProvider.fromSize(
                  size: MediaQuery.of(context).size,
                  child: child,
                ),
              ),
            );
          },
        ),
      );
}
