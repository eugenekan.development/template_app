import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:kreios_app/service/app_secure_storage.dart';

const _tokenKey = 'token';

@injectable
class AuthRepository {
  const AuthRepository(this._storage);

  final AppSecureStorage _storage;

  Future<Option<String>> getToken() async {
    final value = await _storage.read(_tokenKey);
    return optionOf(value);
  }

  Future<void> setToken(String token) => _storage.write(_tokenKey, token);

  Future<void> deleteToken() => _storage.delete(_tokenKey);
}
