import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:kreios_app/feature/theme/color_scheme.dart';

@injectable
class ThemeService {
//  const ThemeService(this._colorsScheme);
  const ThemeService();

//  final ColorsScheme _colorsScheme;
  ThemeData get defaultTheme => _buildDarkData();

  ThemeData _buildLightData() {
    final theme = ThemeData(
      primaryColor: ColorsScheme.colorPrimaryLight,
    );
    return theme.copyWith(colorScheme: _colorScheme(theme));
  }

  ThemeData _buildDarkData() {
    final theme = ThemeData(
      primaryColor: ColorsScheme.colorPrimaryDark,
    );
    return theme.copyWith(colorScheme: _colorScheme(theme));
  }

  ColorScheme _colorScheme(ThemeData theme) => theme.colorScheme.copyWith(
        onSurface: ColorsScheme.colorPrimary,
      );

  ThemeData getTheme(String theme) =>
      theme == darkThemeString ? _buildDarkData() : _buildLightData();
}
