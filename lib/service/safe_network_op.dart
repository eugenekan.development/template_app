import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:kreios_app/service/exception/network_exception.dart';

typedef NetworkOperation<T> = Future<T> Function();

Future<Option<Either<Exception, T>>> safeNetworkOperation<T>(
  NetworkOperation<T> operation,
) async {
  try {
    final response = await operation();
    return optionOf(right(response));
  } on DioError catch (error) {
    print('safeNetworkOperation => DioError => $error');
    return some(left(
      NetworkException(
        statusCode: error.response?.statusCode,
        message: error.response?.statusMessage,
      ),
    ));
  } catch (error) {
    print('safeNetworkOperation => error => $error');
    return some(left(
      error is Exception ? error : Exception(error),
    ));
  }
}
