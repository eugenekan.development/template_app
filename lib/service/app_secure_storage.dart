import 'package:flutter_secured_storage/flutter_secured_storage.dart';
import 'package:injectable/injectable.dart';

@injectable
class AppSecureStorage {
  const AppSecureStorage(this._securedStorage);

  final FlutterSecuredStorage _securedStorage;

  Future<void> write(String key, String value) =>
      _securedStorage.write(key: key, value: value);

  Future<String> read(String key) => _securedStorage.read(key: key);

  Future<void> clearAll() => _securedStorage.deleteAll();

  Future<void> delete(String key) => _securedStorage.delete(key: key);
}
