class NetworkException implements Exception {
  NetworkException({this.statusCode, this.message});

  final int statusCode;
  final String message;
}
