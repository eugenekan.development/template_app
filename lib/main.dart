import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:kreios_app/application/application.dart';
import 'package:kreios_app/injection/injection.dart';

void main() {
  configureInjection(environment: Environment.prod);

  runApp(const MyApp());
}
