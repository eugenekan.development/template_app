import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kreios_app/feature/home/bloc/home_bloc.dart';
import 'package:kreios_app/injection/injection.dart';
import 'package:kreios_app/localization/k_localization.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => BlocProvider<HomeBloc>(
        create: (_) => getIt<HomeBloc>(),
        child: Builder(
          builder: (context) => Scaffold(
            appBar: AppBar(
              title: Text(
                KLocalization.of(context).home_screen.title,
              ),
            ),
            body: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                FlatButton(
                  onPressed: () => context.read<HomeBloc>().add(
                        const HomeEvent.logOut(),
                      ),
                  child: Text(
                      KLocalization.of(context).home_screen.log_out_button),
                ),
              ],
            ),
          ),
        ),
      );
}
