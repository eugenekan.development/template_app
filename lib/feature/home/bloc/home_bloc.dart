import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:kreios_app/router/app_router.gr.dart';
import 'package:kreios_app/service/repository/auth_repository.dart';

part 'home_event.dart';
part 'home_state.dart';
part 'home_bloc.freezed.dart';

@injectable
class HomeBloc extends Bloc<HomeEvent, HomeState> {
  HomeBloc(
    this._appRouter,
    this._authRepository,
  ) : super(const _Initial()) {
//    add(const LocalizationEvent.load());
  }

  final AppRouter _appRouter;
  final AuthRepository _authRepository;

  @override
  Stream<HomeState> mapEventToState(
    HomeEvent event,
  ) async* {
    yield* event.when(
      logOut: _logOut,
    );
  }

  Stream<HomeState> _logOut() async* {
    await _authRepository.deleteToken();
    await _appRouter.replace(const WelcomeScreenRoute());
  }
}
