import 'package:flutter/material.dart';
import 'package:kreios_app/utils/dimensions/dimension_provider.dart';
import 'package:kreios_app/utils/res.dart';

class ModuleContainerWrapper extends StatelessWidget {
  const ModuleContainerWrapper({
    Key key,
    this.child,
  }) : super(key: key);
  final Widget child;

  @override
  Widget build(BuildContext context) {
    final dimensions = DimensionsProvider.of(context).dimensions;
    return Container(
      margin: EdgeInsets.all(dimensions.s),
      padding: EdgeInsets.all(dimensions.m),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: featureBagColor,
      ),
      child: child,
    );
  }
}
