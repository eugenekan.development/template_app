import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:kreios_app/router/app_router.gr.dart';

part 'settings_event.dart';
part 'settings_state.dart';
part 'settings_bloc.freezed.dart';

@injectable
class SettingsBloc extends Bloc<SettingsEvent, SettingsState> {
  SettingsBloc(this._appRouter) : super(const _Initial()) {
//    add(const LocalizationEvent.load());
  }
  final AppRouter _appRouter;

  @override
  Stream<SettingsState> mapEventToState(
    SettingsEvent event,
  ) async* {
    yield* event.when(pop: _pop);
  }

  Stream<SettingsState> _pop() async* {
    await _appRouter.pop();
  }
}
