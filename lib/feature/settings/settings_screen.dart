import 'package:flutter/material.dart';
import 'package:kreios_app/feature/settings/bloc/settings_bloc.dart';
import 'package:kreios_app/feature/theme/bloc/theme_bloc.dart';
import 'package:kreios_app/feature/theme/color_scheme.dart';
import 'package:kreios_app/injection/injection.dart';
import 'package:kreios_app/localization/k_localization.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kreios_app/bloc/localization/localization_bloc.dart';

class SettingsScreen extends StatelessWidget {
  const SettingsScreen({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => BlocProvider(
        create: (_) => getIt<SettingsBloc>(),
        child: Scaffold(
          appBar: AppBar(
            title: Text(KLocalization.of(context).settings_screen.title),
          ),
          body: Column(
            children: [
              BlocBuilder<ThemeBloc, ThemeState>(
                builder: (context, state) => Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Text(
                      KLocalization.of(context).settings_screen.change_theme,
                    ),
                    Switch(
                      activeColor: Theme.of(context).primaryColor,
                      value: state.theme == darkThemeString,
                      onChanged: (value) {
                        context.read<ThemeBloc>().add(
                              value
                                  ? const ThemeEvent.setDarkTheme()
                                  : const ThemeEvent.setDefaultTheme(),
                            );
                      },
                    ),
                  ],
                ),
              ),
              BlocBuilder<LocalizationBloc, LocalizationState>(
                builder: (context, state) => Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Text(
                      KLocalization.of(context).settings_screen.change_language,
                    ),
                    DropdownButton<String>(
                      value: state.locale.languageCode,
                      items: KLocalization.supportedLocales
                          .map(
                            (value) => DropdownMenuItem<String>(
                              value: value.languageCode,
                              child: Text(value.languageCode),
                            ),
                          )
                          .toList(),
                      onChanged: (value) {
                        context.read<LocalizationBloc>().add(
                            LocalizationEvent.changeLanguage(Locale(value)));
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      );
}
