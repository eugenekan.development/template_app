import 'package:flutter/material.dart';

class ColorsScheme {
  ColorsScheme._();

  static const colorPrimaryLight = Color(0xFF00a64b);
  static const colorPrimary = Color(0xFF07662d);
  static const colorPrimaryDark = Color(0xFF08411f);

  static const colorSecondaryLight = Color(0xFFffbd4f);
  static const colorSecondary = Color(0xFFf07f55);
  static const colorSecondaryDark = Color(0xFF96644f);

  static const colorLightGreen = Color(0xFF8dc451);
  static const colorPurple = Color(0xFF6e77d7);
  static const colorBlue = Color(0xFF4caed7);
  static const colorGrey = Color(0xFFdfdfdf);
  static const colorDarkGrey = Color(0xFF4c4c4c);
  static const colorError = Color(0xFFe35252);
  static const colorSurface = Color(0xFFffffff);
  static const colorBackground = Color(0xFFf7f7f7);
  static const colorOnBackground = Color(0xFF272727);

  static const colorYellow = Color(0xFFfec324);

  static const colorAdditionalBlue = Color(0xFF54c5f4);
  static const colorAdditionalGreen = Color(0xFF8dc451);
  static const colorAdditionalPurple = Color(0xFF9ca4ff);
}

const String darkThemeString = 'dark';
const String lightThemeString = 'light';
