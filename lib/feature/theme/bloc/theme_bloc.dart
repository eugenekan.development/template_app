import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secured_storage/flutter_secured_storage.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:kreios_app/feature/theme/color_scheme.dart';
import 'package:kreios_app/service/theme_service.dart';

part 'theme_event.dart';
part 'theme_state.dart';
part 'theme_bloc.freezed.dart';

@injectable
class ThemeBloc extends Bloc<ThemeEvent, ThemeState> {
  ThemeBloc(
    this._flutterSecuredStorage,
    this._themeService,
  ) : super(const ThemeInitialState()) {
    add(const ThemeEvent.load());
  }

  final FlutterSecuredStorage _flutterSecuredStorage;
  final ThemeService _themeService;

  @override
  Stream<ThemeState> mapEventToState(
    ThemeEvent event,
  ) async* {
    yield* event.when(
      load: _load,
      setDarkTheme: _setDarkTheme,
      setDefaultTheme: _setDefaultTheme,
      checkSelectedTheme: _checkSelectedTheme,
      changeTheme: _changeTheme,
    );
  }

  Stream<ThemeState> _load() async* {
    final _theme = await _flutterSecuredStorage.read(
      key: 'theme',
    );

    if (_theme == null) {
      _setDarkTheme();
    } else {
      yield SetThemeState(
        theme: _theme,
        themeData: _themeService.getTheme(_theme),
      );
    }
  }

  Stream<ThemeState> _checkSelectedTheme() async* {}

  Stream<ThemeState> _changeTheme(String some) async* {}

  Stream<ThemeState> _setDarkTheme() async* {
    await _flutterSecuredStorage.write(key: 'theme', value: darkThemeString);

    yield SetThemeState(
      theme: darkThemeString,
      themeData: _themeService.getTheme(darkThemeString),
    );
  }

  Stream<ThemeState> _setDefaultTheme() async* {
    await _flutterSecuredStorage.write(key: 'theme', value: lightThemeString);

    yield SetThemeState(
      theme: lightThemeString,
      themeData: _themeService.getTheme(lightThemeString),
    );
  }
}
