part of 'theme_bloc.dart';

@freezed
abstract class ThemeState with _$ThemeState {
//  const factory ThemeState({
//    @Default(darkThemeString) String theme,
//    ThemeData themeData,
//  }) = _State;
//
  const factory ThemeState.initial({
    @Default(darkThemeString) String theme,
  }) = ThemeInitialState;
//  const factory ThemeState.load() = ThemeLoadState;
//  const factory ThemeState.lightTheme() = ThemeLightThemeState;
//  const factory ThemeState.darkTheme() = ThemeDarkThemeState;
  const factory ThemeState.setTheme({
    @Default(darkThemeString) String theme,
    ThemeData themeData,
  }) = SetThemeState;
}
