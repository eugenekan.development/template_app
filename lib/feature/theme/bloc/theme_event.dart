part of 'theme_bloc.dart';

@freezed
abstract class ThemeEvent with _$ThemeEvent {
  const factory ThemeEvent.checkSelectedTheme() = _SheckSelectedTheme;
  const factory ThemeEvent.setDefaultTheme() = _SetDefaultTheme;
  const factory ThemeEvent.setDarkTheme() = _SetDarkTheme;

  const factory ThemeEvent.changeTheme(String theme) = _ChangeTheme;
  const factory ThemeEvent.load() = _LoadTheme;
}
