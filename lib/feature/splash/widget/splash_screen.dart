import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kreios_app/bloc/auth/auth_bloc.dart';
import 'package:transparent_image/transparent_image.dart';
//import 'package:my_garden_app/bloc/auth/auth_bloc.dart';
//import 'package:scotts_widget/style/animation_length.dart';
//import 'package:transparent_image/transparent_image.dart';

const splashImageHeroTag = 'SplashImage';

class SplashScreen extends StatefulWidget {
  const SplashScreen({
    Key key,
  }) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  Image _image;

  @override
  void initState() {
    super.initState();
    _image = Image.asset('assets/images/splash.png');
    WidgetsBinding.instance.addPostFrameCallback(
        (_) => context.read<AuthBloc>().add(const AuthEvent.checkToken()));
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    precacheImage(_image.image, context);
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        body: SizedBox.expand(
          child: Hero(
            tag: splashImageHeroTag,
            child: FadeInImage(
//              fadeInDuration: SCAnimationLength.short,
              image: _image.image,
              placeholder: MemoryImage(kTransparentImage),
              fit: BoxFit.fill,
            ),
          ),
        ),
      );
}
