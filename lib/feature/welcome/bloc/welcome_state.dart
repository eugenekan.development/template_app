part of 'welcome_bloc.dart';

@freezed
abstract class WelcomeState with _$WelcomeState {
  const factory WelcomeState() = _Initial;
}
