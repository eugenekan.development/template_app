// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'welcome_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$WelcomeEventTearOff {
  const _$WelcomeEventTearOff();

// ignore: unused_element
  _NavigateToSettings navigateToSettings() {
    return const _NavigateToSettings();
  }

// ignore: unused_element
  _LogIn logIn() {
    return const _LogIn();
  }

// ignore: unused_element
  _signUp signUp() {
    return const _signUp();
  }
}

/// @nodoc
// ignore: unused_element
const $WelcomeEvent = _$WelcomeEventTearOff();

/// @nodoc
mixin _$WelcomeEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult navigateToSettings(),
    @required TResult logIn(),
    @required TResult signUp(),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult navigateToSettings(),
    TResult logIn(),
    TResult signUp(),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult navigateToSettings(_NavigateToSettings value),
    @required TResult logIn(_LogIn value),
    @required TResult signUp(_signUp value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult navigateToSettings(_NavigateToSettings value),
    TResult logIn(_LogIn value),
    TResult signUp(_signUp value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $WelcomeEventCopyWith<$Res> {
  factory $WelcomeEventCopyWith(
          WelcomeEvent value, $Res Function(WelcomeEvent) then) =
      _$WelcomeEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$WelcomeEventCopyWithImpl<$Res> implements $WelcomeEventCopyWith<$Res> {
  _$WelcomeEventCopyWithImpl(this._value, this._then);

  final WelcomeEvent _value;
  // ignore: unused_field
  final $Res Function(WelcomeEvent) _then;
}

/// @nodoc
abstract class _$NavigateToSettingsCopyWith<$Res> {
  factory _$NavigateToSettingsCopyWith(
          _NavigateToSettings value, $Res Function(_NavigateToSettings) then) =
      __$NavigateToSettingsCopyWithImpl<$Res>;
}

/// @nodoc
class __$NavigateToSettingsCopyWithImpl<$Res>
    extends _$WelcomeEventCopyWithImpl<$Res>
    implements _$NavigateToSettingsCopyWith<$Res> {
  __$NavigateToSettingsCopyWithImpl(
      _NavigateToSettings _value, $Res Function(_NavigateToSettings) _then)
      : super(_value, (v) => _then(v as _NavigateToSettings));

  @override
  _NavigateToSettings get _value => super._value as _NavigateToSettings;
}

/// @nodoc
class _$_NavigateToSettings implements _NavigateToSettings {
  const _$_NavigateToSettings();

  @override
  String toString() {
    return 'WelcomeEvent.navigateToSettings()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _NavigateToSettings);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult navigateToSettings(),
    @required TResult logIn(),
    @required TResult signUp(),
  }) {
    assert(navigateToSettings != null);
    assert(logIn != null);
    assert(signUp != null);
    return navigateToSettings();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult navigateToSettings(),
    TResult logIn(),
    TResult signUp(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (navigateToSettings != null) {
      return navigateToSettings();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult navigateToSettings(_NavigateToSettings value),
    @required TResult logIn(_LogIn value),
    @required TResult signUp(_signUp value),
  }) {
    assert(navigateToSettings != null);
    assert(logIn != null);
    assert(signUp != null);
    return navigateToSettings(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult navigateToSettings(_NavigateToSettings value),
    TResult logIn(_LogIn value),
    TResult signUp(_signUp value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (navigateToSettings != null) {
      return navigateToSettings(this);
    }
    return orElse();
  }
}

abstract class _NavigateToSettings implements WelcomeEvent {
  const factory _NavigateToSettings() = _$_NavigateToSettings;
}

/// @nodoc
abstract class _$LogInCopyWith<$Res> {
  factory _$LogInCopyWith(_LogIn value, $Res Function(_LogIn) then) =
      __$LogInCopyWithImpl<$Res>;
}

/// @nodoc
class __$LogInCopyWithImpl<$Res> extends _$WelcomeEventCopyWithImpl<$Res>
    implements _$LogInCopyWith<$Res> {
  __$LogInCopyWithImpl(_LogIn _value, $Res Function(_LogIn) _then)
      : super(_value, (v) => _then(v as _LogIn));

  @override
  _LogIn get _value => super._value as _LogIn;
}

/// @nodoc
class _$_LogIn implements _LogIn {
  const _$_LogIn();

  @override
  String toString() {
    return 'WelcomeEvent.logIn()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _LogIn);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult navigateToSettings(),
    @required TResult logIn(),
    @required TResult signUp(),
  }) {
    assert(navigateToSettings != null);
    assert(logIn != null);
    assert(signUp != null);
    return logIn();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult navigateToSettings(),
    TResult logIn(),
    TResult signUp(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (logIn != null) {
      return logIn();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult navigateToSettings(_NavigateToSettings value),
    @required TResult logIn(_LogIn value),
    @required TResult signUp(_signUp value),
  }) {
    assert(navigateToSettings != null);
    assert(logIn != null);
    assert(signUp != null);
    return logIn(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult navigateToSettings(_NavigateToSettings value),
    TResult logIn(_LogIn value),
    TResult signUp(_signUp value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (logIn != null) {
      return logIn(this);
    }
    return orElse();
  }
}

abstract class _LogIn implements WelcomeEvent {
  const factory _LogIn() = _$_LogIn;
}

/// @nodoc
abstract class _$signUpCopyWith<$Res> {
  factory _$signUpCopyWith(_signUp value, $Res Function(_signUp) then) =
      __$signUpCopyWithImpl<$Res>;
}

/// @nodoc
class __$signUpCopyWithImpl<$Res> extends _$WelcomeEventCopyWithImpl<$Res>
    implements _$signUpCopyWith<$Res> {
  __$signUpCopyWithImpl(_signUp _value, $Res Function(_signUp) _then)
      : super(_value, (v) => _then(v as _signUp));

  @override
  _signUp get _value => super._value as _signUp;
}

/// @nodoc
class _$_signUp implements _signUp {
  const _$_signUp();

  @override
  String toString() {
    return 'WelcomeEvent.signUp()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _signUp);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult navigateToSettings(),
    @required TResult logIn(),
    @required TResult signUp(),
  }) {
    assert(navigateToSettings != null);
    assert(logIn != null);
    assert(signUp != null);
    return signUp();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult navigateToSettings(),
    TResult logIn(),
    TResult signUp(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (signUp != null) {
      return signUp();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult navigateToSettings(_NavigateToSettings value),
    @required TResult logIn(_LogIn value),
    @required TResult signUp(_signUp value),
  }) {
    assert(navigateToSettings != null);
    assert(logIn != null);
    assert(signUp != null);
    return signUp(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult navigateToSettings(_NavigateToSettings value),
    TResult logIn(_LogIn value),
    TResult signUp(_signUp value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (signUp != null) {
      return signUp(this);
    }
    return orElse();
  }
}

abstract class _signUp implements WelcomeEvent {
  const factory _signUp() = _$_signUp;
}

/// @nodoc
class _$WelcomeStateTearOff {
  const _$WelcomeStateTearOff();

// ignore: unused_element
  _Initial call() {
    return const _Initial();
  }
}

/// @nodoc
// ignore: unused_element
const $WelcomeState = _$WelcomeStateTearOff();

/// @nodoc
mixin _$WelcomeState {}

/// @nodoc
abstract class $WelcomeStateCopyWith<$Res> {
  factory $WelcomeStateCopyWith(
          WelcomeState value, $Res Function(WelcomeState) then) =
      _$WelcomeStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$WelcomeStateCopyWithImpl<$Res> implements $WelcomeStateCopyWith<$Res> {
  _$WelcomeStateCopyWithImpl(this._value, this._then);

  final WelcomeState _value;
  // ignore: unused_field
  final $Res Function(WelcomeState) _then;
}

/// @nodoc
abstract class _$InitialCopyWith<$Res> {
  factory _$InitialCopyWith(_Initial value, $Res Function(_Initial) then) =
      __$InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$InitialCopyWithImpl<$Res> extends _$WelcomeStateCopyWithImpl<$Res>
    implements _$InitialCopyWith<$Res> {
  __$InitialCopyWithImpl(_Initial _value, $Res Function(_Initial) _then)
      : super(_value, (v) => _then(v as _Initial));

  @override
  _Initial get _value => super._value as _Initial;
}

/// @nodoc
class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'WelcomeState()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;
}

abstract class _Initial implements WelcomeState {
  const factory _Initial() = _$_Initial;
}
