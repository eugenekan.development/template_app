part of 'welcome_bloc.dart';

@freezed
abstract class WelcomeEvent with _$WelcomeEvent {
  const factory WelcomeEvent.navigateToSettings() = _NavigateToSettings;
  const factory WelcomeEvent.logIn() = _LogIn;
  const factory WelcomeEvent.signUp() = _signUp;
}
