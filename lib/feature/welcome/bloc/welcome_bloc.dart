import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:kreios_app/router/app_router.gr.dart';

part 'welcome_event.dart';
part 'welcome_state.dart';
part 'welcome_bloc.freezed.dart';

@injectable
class WelcomeBloc extends Bloc<WelcomeEvent, WelcomeState> {
  WelcomeBloc(this._appRouter) : super(const _Initial()) {
//    add(const LocalizationEvent.load());
  }
  final AppRouter _appRouter;

  @override
  Stream<WelcomeState> mapEventToState(
    WelcomeEvent event,
  ) async* {
    yield* event.when(
      navigateToSettings: _goToSettings,
      signUp: _signUp,
      logIn: _logIn,
    );
  }

  Stream<WelcomeState> _goToSettings() async* {
    await _appRouter.push(const SettingsScreenRoute());
  }

  Stream<WelcomeState> _signUp() async* {
    await _appRouter.push(const UnderConstructionRoute());
  }

  Stream<WelcomeState> _logIn() async* {
    await _appRouter.push(const LogInScreenRoute());
  }
}
