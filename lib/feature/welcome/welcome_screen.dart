import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kreios_app/feature/welcome/bloc/welcome_bloc.dart';
import 'package:kreios_app/injection/injection.dart';
import 'package:kreios_app/localization/k_localization.dart';

class WelcomeScreen extends StatelessWidget {
  const WelcomeScreen({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => BlocProvider(
        create: (_) => getIt<WelcomeBloc>(),
        child: Builder(
          builder: (context) {
            final localization = KLocalization.of(context).welcome_screen;
            return Scaffold(
              appBar: AppBar(
                title: Text(
                  localization.title,
                ),
              ),
              body: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  FlatButton(
                    onPressed: () => context.read<WelcomeBloc>().add(
                          const WelcomeEvent.logIn(),
                        ),
                    child: Text(
                      localization.log_in_button,
                    ),
                  ),
                  RaisedButton(
                    onPressed: () => context.read<WelcomeBloc>().add(
                          const WelcomeEvent.signUp(),
                        ),
                    child: Text(
                      localization.sign_up_button,
                    ),
                  ),
                  TextButton(
                    onPressed: () => context
                        .read<WelcomeBloc>()
                        .add(const WelcomeEvent.navigateToSettings()),
                    child: Text(
                      KLocalization.of(context).welcome_screen.settings,
                    ),
                  ),
                ],
              ),
            );
          },
        ),
      );
}
