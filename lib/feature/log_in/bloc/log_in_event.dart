part of 'log_in_bloc.dart';

@freezed
abstract class LogInEvent with _$LogInEvent {
  const factory LogInEvent.updateEmail(String email) = _UpdateEmail;
  const factory LogInEvent.updatePassword(String password) = _UpdatePassword;

  const factory LogInEvent.logIn() = _LogIn;
  const factory LogInEvent.pop() = _Pop;
}
