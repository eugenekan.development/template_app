import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:kreios_app/feature/log_in/service/log_in_repository.dart';
import 'package:kreios_app/router/app_router.gr.dart';
import 'package:kreios_app/service/repository/auth_repository.dart';

part 'log_in_event.dart';
part 'log_in_state.dart';
part 'log_in_bloc.freezed.dart';

@injectable
class LogInBloc extends Bloc<LogInEvent, LogInState> {
  LogInBloc(
    this._appRouter,
    this._logInRepository,
    this._authRepository,
  ) : super(const _Initial()) {
//    add(const LocalizationEvent.load());
  }

  final AppRouter _appRouter;
  final LogInRepository _logInRepository;
  final AuthRepository _authRepository;

  @override
  Stream<LogInState> mapEventToState(
    LogInEvent event,
  ) async* {
    yield* event.when(
      updateEmail: _updateEmail,
      updatePassword: _updatePassword,
      logIn: _logIn,
      pop: _pop,
    );
  }

  Stream<LogInState> _pop() async* {
    await _appRouter.pop();
  }

  Stream<LogInState> _updatePassword(String password) async* {
    yield state.copyWith(
      isPasswordValid: _passwordValidation(password),
      password: password,
    );
  }

  Stream<LogInState> _logIn() async* {
    final response = await _logInRepository.logIn(state.email, state.password);

    //TODO: add few more states for login staging
    response.fold(
      () => print('ERROR'),
      (a) => a.fold(
        (l) => print('exception=> ${l.toString()}'),
        (r) => r == 'success' ? _successLogin() : _errorLogin(),
      ),
    );
  }

//TODO: add success login logic
  void _successLogin() {
    _authRepository.setToken('asdasd');
    _appRouter.pushAndRemoveUntil(const HomeScreenRoute(),
        predicate: (route) => false);
  }

  void _errorLogin() {
    _appRouter.replace(const UnderConstructionRoute());
  }

  Stream<LogInState> _updateEmail(String email) async* {
    yield state.copyWith(
      isEmailValid: _emailValidation(email),
      email: email,
    );
  }

  bool _emailValidation(String email) {
    var emailValid = RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(email);

    if (email.isEmpty) {
      emailValid = true;
    }

    return emailValid;
  }

  bool _passwordValidation(String password) {
    var passwordValid = true;

    if (password.length > 5) {
      passwordValid = false;
    }

    return passwordValid;
  }
}
