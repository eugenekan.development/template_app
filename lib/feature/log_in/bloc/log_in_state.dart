part of 'log_in_bloc.dart';

@freezed
abstract class LogInState with _$LogInState {
  const factory LogInState({
    @Default(true) bool isEmailValid,
    @Default(true) bool isPasswordValid,
    @Default('') String email,
    @Default('') String password,
  }) = _Initial;
}
