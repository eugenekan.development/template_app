import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:retrofit/retrofit.dart';

part 'log_in_api.g.dart';

@lazySingleton
@RestApi()
abstract class LogInApi {
  @factoryMethod
  factory LogInApi(Dio dio) = _LogInApi;

  @GET('/log_in')
  Future<String> logIn({
    @Query('email') String email,
    @Query('password') String password,
  });
}
