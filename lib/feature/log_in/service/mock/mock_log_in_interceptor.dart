import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:kreios_app/feature/log_in/service/mock/mock_log_in_response.dart';
import 'package:kreios_app/injection/injection.dart';
//import 'package:my_garden_app/feature/tips/service/mock/mock_filters_response.dart';
//import 'package:my_garden_app/feature/tips/service/mock/mock_tips_response.dart';
//import 'package:my_garden_app/injection/injection.dart';

@injectable
class MockLogInInterceptor extends Interceptor {
  @override
  Future<dynamic> onRequest(RequestOptions options) async {
    if (options.path == '/log_in') {
      print('MockLogInInterceptor => onRequest ${options.path}');
      if (options.queryParameters["email"] == 'valid@test.com') {
        return getIt<Dio>().resolve<String>(mockLogInSuccessResponse);
      } else {
        return getIt<Dio>().resolve<String>(mockLogInErrorResponse);
      }
    }

    return options;
  }
}
