import 'package:dartz/dartz.dart';
import 'package:kreios_app/feature/log_in/service/log_in_api.dart';
import 'package:injectable/injectable.dart';
import 'package:kreios_app/service/safe_network_op.dart';

@injectable
class LogInRepository {
  LogInRepository(this._logInApi);

  final LogInApi _logInApi;

  Future<Option<Either<Exception, String>>> logIn(
    String email,
    String password,
  ) =>
      safeNetworkOperation(
        () => _logInApi.logIn(email: email, password: password),
      );
}
