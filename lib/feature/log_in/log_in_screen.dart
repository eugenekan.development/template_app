import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kreios_app/feature/log_in/bloc/log_in_bloc.dart';
import 'package:kreios_app/feature/log_in/widget/app_input.dart';
import 'package:kreios_app/injection/injection.dart';
import 'package:kreios_app/localization/k_localization.dart';

class LogInScreen extends StatelessWidget {
  const LogInScreen({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => BlocProvider(
        create: (_) => getIt<LogInBloc>(),
        child: BlocBuilder<LogInBloc, LogInState>(
          builder: (context, state) {
            final localization = KLocalization.of(context).welcome_screen;
            return Scaffold(
              appBar: AppBar(
                title: Text(
                  localization.title,
                ),
              ),
              body: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text('valid@test.com'),
                  AppTextInputField(
                    onChanged: (str) => context
                        .read<LogInBloc>()
                        .add(LogInEvent.updateEmail(str)),
//                    labelText: ,
                    errorMessage: state.isEmailValid ? null : 'not valid',
                    hintText: 'email',
                  ),
                  AppTextInputField(
                    onChanged: (str) => context
                        .read<LogInBloc>()
                        .add(LogInEvent.updatePassword(str)),
//                    labelText: ,
                    errorMessage: state.isPasswordValid ? null : 'not valid',
                    hintText: 'password',
                  ),
                  FlatButton(
                    onPressed: () => context.read<LogInBloc>().add(
                          const LogInEvent.logIn(),
                        ),
                    child: Text(
                      localization.log_in_button,
                    ),
                  ),
                ],
              ),
            );
          },
        ),
      );
}
