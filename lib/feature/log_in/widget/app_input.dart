import 'package:flutter/material.dart';

class AppTextInputField extends StatefulWidget {
  const AppTextInputField({
    @required this.onChanged,
    this.errorMessage,
    this.labelText,
    this.hintText,
    Key key,
  }) : super(key: key);

  final void Function(String) onChanged;
  final String errorMessage;
  final String labelText;
  final String hintText;

  @override
  _AppTextInputFieldState createState() => _AppTextInputFieldState();
}

class _AppTextInputFieldState extends State<AppTextInputField> {
  final TextEditingController _appController = TextEditingController();

  @override
  void dispose() {
    _appController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) => Row(
        children: <Widget>[
          Expanded(
            child: TextField(
              autofocus: true,
              onChanged: widget.onChanged,
              keyboardType: TextInputType.number,
              controller: _appController,
              style: Theme.of(context)
                  .textTheme
                  .bodyText1
                  .copyWith(fontWeight: FontWeight.w400),
              decoration: InputDecoration(
                labelText: widget.labelText,
//                  suffixText: state.address.addressString,
                errorText: widget.errorMessage,
                hintText: widget.hintText,
              ),
            ),
          ),
        ],
      );

//  void _setInitialValue(ZipState state) {
//    if (state.zipCode.isNone()) {
//      _appController.text = state.initialZipCode.fold(
//            () => '',
//            (code) => code,
//      );
//      _appController.selection = TextSelection.fromPosition(
//        TextPosition(offset: _appController.text.length),
//      );
//    }
//  }
}
