import 'package:injectable/injectable.dart';
import 'package:kreios_app/router/app_router.gr.dart';

@module
abstract class BlocModule {
  @lazySingleton
  AppRouter get router => AppRouter();
}
