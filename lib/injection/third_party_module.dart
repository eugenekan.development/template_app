import 'package:dio/dio.dart';
import 'package:flutter_secured_storage/flutter_secured_storage.dart';
import 'package:injectable/injectable.dart';
import 'package:kreios_app/feature/log_in/service/mock/mock_log_in_interceptor.dart';

@module
abstract class ThirdPartyModule {
  @lazySingleton
  FlutterSecuredStorage get flutterSecuredStorage =>
      const FlutterSecuredStorage();

  @lazySingleton
  Dio dio(BaseOptions options) => Dio(options)
    ..interceptors.addAll([
      MockLogInInterceptor(),
    ]);

  @lazySingleton
  BaseOptions dioOptions(@Named('baseUrl') String baseUrl) => BaseOptions(
        baseUrl: baseUrl,
      );

  @singleton
  @Named('baseUrl')
  String get baseUrl => 'https://test.com/api';
}
