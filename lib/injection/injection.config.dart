// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:dio/dio.dart' as _i12;
import 'package:flutter_secured_storage/flutter_secured_storage.dart' as _i4;
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;

import '../bloc/auth/auth_bloc.dart' as _i17;
import '../bloc/localization/localization_bloc.dart' as _i5;
import '../feature/home/bloc/home_bloc.dart' as _i13;
import '../feature/log_in/bloc/log_in_bloc.dart' as _i18;
import '../feature/log_in/service/log_in_api.dart' as _i14;
import '../feature/log_in/service/log_in_repository.dart' as _i15;
import '../feature/log_in/service/mock/mock_log_in_interceptor.dart' as _i6;
import '../feature/settings/bloc/settings_bloc.dart' as _i7;
import '../feature/theme/bloc/theme_bloc.dart' as _i16;
import '../feature/welcome/bloc/welcome_bloc.dart' as _i9;
import '../router/app_router.gr.dart' as _i3;
import '../service/app_secure_storage.dart' as _i10;
import '../service/repository/auth_repository.dart' as _i11;
import '../service/theme_service.dart' as _i8;
import 'bloc_module.dart' as _i19;
import 'third_party_module.dart'
    as _i20; // ignore_for_file: unnecessary_lambdas

// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
_i1.GetIt $initGetIt(_i1.GetIt get,
    {String environment, _i2.EnvironmentFilter environmentFilter}) {
  final gh = _i2.GetItHelper(get, environment, environmentFilter);
  final blocModule = _$BlocModule();
  final thirdPartyModule = _$ThirdPartyModule();
  gh.lazySingleton<_i3.AppRouter>(() => blocModule.router);
  gh.lazySingleton<_i4.FlutterSecuredStorage>(
      () => thirdPartyModule.flutterSecuredStorage);
  gh.factory<_i5.LocalizationBloc>(
      () => _i5.LocalizationBloc(get<_i4.FlutterSecuredStorage>()));
  gh.factory<_i6.MockLogInInterceptor>(() => _i6.MockLogInInterceptor());
  gh.factory<_i7.SettingsBloc>(() => _i7.SettingsBloc(get<_i3.AppRouter>()));
  gh.factory<_i8.ThemeService>(() => _i8.ThemeService());
  gh.factory<_i9.WelcomeBloc>(() => _i9.WelcomeBloc(get<_i3.AppRouter>()));
  gh.factory<_i10.AppSecureStorage>(
      () => _i10.AppSecureStorage(get<_i4.FlutterSecuredStorage>()));
  gh.factory<_i11.AuthRepository>(
      () => _i11.AuthRepository(get<_i10.AppSecureStorage>()));
  gh.lazySingleton<_i12.BaseOptions>(
      () => thirdPartyModule.dioOptions(get<String>(instanceName: 'baseUrl')));
  gh.lazySingleton<_i12.Dio>(
      () => thirdPartyModule.dio(get<_i12.BaseOptions>()));
  gh.factory<_i13.HomeBloc>(
      () => _i13.HomeBloc(get<_i3.AppRouter>(), get<_i11.AuthRepository>()));
  gh.lazySingleton<_i14.LogInApi>(() => _i14.LogInApi(get<_i12.Dio>()));
  gh.factory<_i15.LogInRepository>(
      () => _i15.LogInRepository(get<_i14.LogInApi>()));
  gh.factory<_i16.ThemeBloc>(() => _i16.ThemeBloc(
      get<_i4.FlutterSecuredStorage>(), get<_i8.ThemeService>()));
  gh.factory<_i17.AuthBloc>(() => _i17.AuthBloc(
      router: get<_i3.AppRouter>(),
      authRepository: get<_i11.AuthRepository>()));
  gh.factory<_i18.LogInBloc>(() => _i18.LogInBloc(get<_i3.AppRouter>(),
      get<_i15.LogInRepository>(), get<_i11.AuthRepository>()));
  gh.singleton<String>(thirdPartyModule.baseUrl, instanceName: 'baseUrl');
  return get;
}

class _$BlocModule extends _i19.BlocModule {}

class _$ThirdPartyModule extends _i20.ThirdPartyModule {}
