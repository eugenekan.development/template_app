// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

import 'package:auto_route/auto_route.dart' as _i1;

import '../feature/home/home_screen.dart' as _i5;
import '../feature/log_in/log_in_screen.dart' as _i6;
import '../feature/settings/settings_screen.dart' as _i7;
import '../feature/splash/widget/splash_screen.dart' as _i2;
import '../feature/welcome/welcome_screen.dart' as _i4;
import '../widget/under_construction.dart' as _i3;

class AppRouter extends _i1.RootStackRouter {
  AppRouter();

  @override
  final Map<String, _i1.PageFactory> pagesMap = {
    SplashScreenRoute.name: (entry) {
      return _i1.MaterialPageX(entry: entry, child: const _i2.SplashScreen());
    },
    UnderConstructionRoute.name: (entry) {
      return _i1.MaterialPageX(
          entry: entry, child: const _i3.UnderConstruction());
    },
    WelcomeScreenRoute.name: (entry) {
      return _i1.CustomPage(
          entry: entry,
          child: const _i4.WelcomeScreen(),
          transitionsBuilder: _i1.TransitionsBuilders.fadeIn);
    },
    HomeScreenRoute.name: (entry) {
      return _i1.CustomPage(
          entry: entry,
          child: const _i5.HomeScreen(),
          transitionsBuilder: _i1.TransitionsBuilders.fadeIn);
    },
    LogInScreenRoute.name: (entry) {
      return _i1.MaterialPageX(entry: entry, child: const _i6.LogInScreen());
    },
    SettingsScreenRoute.name: (entry) {
      return _i1.MaterialPageX(entry: entry, child: const _i7.SettingsScreen());
    }
  };

  @override
  List<_i1.RouteConfig> get routes => [
        _i1.RouteConfig<SplashScreenRoute>(SplashScreenRoute.name,
            path: '/',
            routeBuilder: (match) => SplashScreenRoute.fromMatch(match)),
        _i1.RouteConfig<UnderConstructionRoute>(UnderConstructionRoute.name,
            path: '/under_construction',
            routeBuilder: (match) => UnderConstructionRoute.fromMatch(match)),
        _i1.RouteConfig<WelcomeScreenRoute>(WelcomeScreenRoute.name,
            path: '/welcome',
            routeBuilder: (match) => WelcomeScreenRoute.fromMatch(match)),
        _i1.RouteConfig<HomeScreenRoute>(HomeScreenRoute.name,
            path: '/home',
            routeBuilder: (match) => HomeScreenRoute.fromMatch(match)),
        _i1.RouteConfig<LogInScreenRoute>(LogInScreenRoute.name,
            path: '/log_in',
            routeBuilder: (match) => LogInScreenRoute.fromMatch(match)),
        _i1.RouteConfig<SettingsScreenRoute>(SettingsScreenRoute.name,
            path: '/settings',
            routeBuilder: (match) => SettingsScreenRoute.fromMatch(match))
      ];
}

class SplashScreenRoute extends _i1.PageRouteInfo {
  const SplashScreenRoute() : super(name, path: '/');

  SplashScreenRoute.fromMatch(_i1.RouteMatch match) : super.fromMatch(match);

  static const String name = 'SplashScreenRoute';
}

class UnderConstructionRoute extends _i1.PageRouteInfo {
  const UnderConstructionRoute() : super(name, path: '/under_construction');

  UnderConstructionRoute.fromMatch(_i1.RouteMatch match)
      : super.fromMatch(match);

  static const String name = 'UnderConstructionRoute';
}

class WelcomeScreenRoute extends _i1.PageRouteInfo {
  const WelcomeScreenRoute() : super(name, path: '/welcome');

  WelcomeScreenRoute.fromMatch(_i1.RouteMatch match) : super.fromMatch(match);

  static const String name = 'WelcomeScreenRoute';
}

class HomeScreenRoute extends _i1.PageRouteInfo {
  const HomeScreenRoute() : super(name, path: '/home');

  HomeScreenRoute.fromMatch(_i1.RouteMatch match) : super.fromMatch(match);

  static const String name = 'HomeScreenRoute';
}

class LogInScreenRoute extends _i1.PageRouteInfo {
  const LogInScreenRoute() : super(name, path: '/log_in');

  LogInScreenRoute.fromMatch(_i1.RouteMatch match) : super.fromMatch(match);

  static const String name = 'LogInScreenRoute';
}

class SettingsScreenRoute extends _i1.PageRouteInfo {
  const SettingsScreenRoute() : super(name, path: '/settings');

  SettingsScreenRoute.fromMatch(_i1.RouteMatch match) : super.fromMatch(match);

  static const String name = 'SettingsScreenRoute';
}
