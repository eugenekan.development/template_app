import 'package:auto_route/auto_route.dart';
import 'package:kreios_app/feature/home/home_screen.dart';
import 'package:kreios_app/feature/log_in/log_in_screen.dart';
import 'package:kreios_app/feature/settings/settings_screen.dart';
import 'package:kreios_app/feature/splash/widget/splash_screen.dart';
import 'package:kreios_app/feature/welcome/welcome_screen.dart';
import 'package:kreios_app/widget/under_construction.dart';

@MaterialAutoRouter(
  routes: <AutoRoute<dynamic>>[
    AutoRoute<void>(page: SplashScreen, initial: true),
    AutoRoute<void>(page: UnderConstruction, path: '/under_construction'),
    CustomRoute<void>(
      page: WelcomeScreen,
      path: '/welcome',
      transitionsBuilder: TransitionsBuilders.fadeIn,
    ),
    CustomRoute<void>(
      page: HomeScreen,
      path: '/home',
      transitionsBuilder: TransitionsBuilders.fadeIn,
    ),
    AutoRoute<void>(page: LogInScreen, path: '/log_in'),
    AutoRoute<void>(page: SettingsScreen, path: '/settings'),
  ],
)
class $AppRouter {}
