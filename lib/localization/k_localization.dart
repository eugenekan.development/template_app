import 'package:flutter/material.dart';
import 'package:kreios_app/localization/translation/translations.i69n.dart';
import 'package:kreios_app/localization/translation/translations_ru.i69n.dart';

const _supportedLocales = ['en', 'ru'];

class KLocalization {
  const KLocalization(this.translations);

  final Translations translations;

  static final _translations = <String, Translations Function()>{
    'en': () => const Translations(),
    'ru': () => const Translations_ru(),
  };

  static const LocalizationsDelegate<KLocalization> delegate =
      _FaunaLocalizationsDelegate();

  static final List<Locale> supportedLocales =
      _supportedLocales.map((x) => Locale(x)).toList();

  static Future<KLocalization> load(Locale locale) =>
      Future.value(KLocalization(_translations[locale.languageCode]()));

  static Translations of(BuildContext context) =>
      Localizations.of<KLocalization>(context, KLocalization).translations;
}

class _FaunaLocalizationsDelegate extends LocalizationsDelegate<KLocalization> {
  const _FaunaLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) =>
      _supportedLocales.contains(locale.languageCode);

  @override
  Future<KLocalization> load(Locale locale) => KLocalization.load(locale);

  @override
  bool shouldReload(LocalizationsDelegate<KLocalization> old) => false;
}
