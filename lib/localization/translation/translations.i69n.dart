// ignore_for_file: unused_element, unused_field, camel_case_types, annotate_overrides, prefer_single_quotes
// GENERATED FILE, do not edit!
import 'package:i69n/i69n.dart' as i69n;

String get _languageCode => 'en';
String get _localeName => 'en';

String _plural(int count,
        {String zero,
        String one,
        String two,
        String few,
        String many,
        String other}) =>
    i69n.plural(count, _languageCode,
        zero: zero, one: one, two: two, few: few, many: many, other: other);
String _ordinal(int count,
        {String zero,
        String one,
        String two,
        String few,
        String many,
        String other}) =>
    i69n.ordinal(count, _languageCode,
        zero: zero, one: one, two: two, few: few, many: many, other: other);
String _cardinal(int count,
        {String zero,
        String one,
        String two,
        String few,
        String many,
        String other}) =>
    i69n.cardinal(count, _languageCode,
        zero: zero, one: one, two: two, few: few, many: many, other: other);

class Translations implements i69n.I69nMessageBundle {
  const Translations();
  Welcome_screenTranslations get welcome_screen =>
      Welcome_screenTranslations(this);
  Home_screenTranslations get home_screen => Home_screenTranslations(this);
  Settings_screenTranslations get settings_screen =>
      Settings_screenTranslations(this);
  Under_constructionTranslations get under_construction =>
      Under_constructionTranslations(this);
  Object operator [](String key) {
    var index = key.indexOf('.');
    if (index > 0) {
      return (this[key.substring(0, index)]
          as i69n.I69nMessageBundle)[key.substring(index + 1)];
    }
    switch (key) {
      case 'welcome_screen':
        return welcome_screen;
      case 'home_screen':
        return home_screen;
      case 'settings_screen':
        return settings_screen;
      case 'under_construction':
        return under_construction;
      default:
        throw Exception('Message $key doesn\'t exist in $this');
    }
  }
}

class Welcome_screenTranslations implements i69n.I69nMessageBundle {
  final Translations _parent;
  const Welcome_screenTranslations(this._parent);
  String get title => "Welcome Screen";
  String get settings => "Settings";
  String get log_in_button => "Log In";
  String get sign_up_button => "Sign Up";
  Object operator [](String key) {
    var index = key.indexOf('.');
    if (index > 0) {
      return (this[key.substring(0, index)]
          as i69n.I69nMessageBundle)[key.substring(index + 1)];
    }
    switch (key) {
      case 'title':
        return title;
      case 'settings':
        return settings;
      case 'log_in_button':
        return log_in_button;
      case 'sign_up_button':
        return sign_up_button;
      default:
        throw Exception('Message $key doesn\'t exist in $this');
    }
  }
}

class Home_screenTranslations implements i69n.I69nMessageBundle {
  final Translations _parent;
  const Home_screenTranslations(this._parent);
  String get title => "Home Screen";
  String get log_out_button => "Log Out";
  Object operator [](String key) {
    var index = key.indexOf('.');
    if (index > 0) {
      return (this[key.substring(0, index)]
          as i69n.I69nMessageBundle)[key.substring(index + 1)];
    }
    switch (key) {
      case 'title':
        return title;
      case 'log_out_button':
        return log_out_button;
      default:
        throw Exception('Message $key doesn\'t exist in $this');
    }
  }
}

class Settings_screenTranslations implements i69n.I69nMessageBundle {
  final Translations _parent;
  const Settings_screenTranslations(this._parent);
  String get title => "Settings screen";
  String get change_theme => "Change theme";
  String get change_language => "Select Language";
  Object operator [](String key) {
    var index = key.indexOf('.');
    if (index > 0) {
      return (this[key.substring(0, index)]
          as i69n.I69nMessageBundle)[key.substring(index + 1)];
    }
    switch (key) {
      case 'title':
        return title;
      case 'change_theme':
        return change_theme;
      case 'change_language':
        return change_language;
      default:
        throw Exception('Message $key doesn\'t exist in $this');
    }
  }
}

class Under_constructionTranslations implements i69n.I69nMessageBundle {
  final Translations _parent;
  const Under_constructionTranslations(this._parent);
  String get title => "UNDER CONSTRUCTION";
  Object operator [](String key) {
    var index = key.indexOf('.');
    if (index > 0) {
      return (this[key.substring(0, index)]
          as i69n.I69nMessageBundle)[key.substring(index + 1)];
    }
    switch (key) {
      case 'title':
        return title;
      default:
        throw Exception('Message $key doesn\'t exist in $this');
    }
  }
}
