// ignore_for_file: unused_element, unused_field, camel_case_types, annotate_overrides, prefer_single_quotes
// GENERATED FILE, do not edit!
import 'package:i69n/i69n.dart' as i69n;
import 'translations.i69n.dart';

String get _languageCode => 'ru';
String get _localeName => 'ru';

String _plural(int count,
        {String zero,
        String one,
        String two,
        String few,
        String many,
        String other}) =>
    i69n.plural(count, _languageCode,
        zero: zero, one: one, two: two, few: few, many: many, other: other);
String _ordinal(int count,
        {String zero,
        String one,
        String two,
        String few,
        String many,
        String other}) =>
    i69n.ordinal(count, _languageCode,
        zero: zero, one: one, two: two, few: few, many: many, other: other);
String _cardinal(int count,
        {String zero,
        String one,
        String two,
        String few,
        String many,
        String other}) =>
    i69n.cardinal(count, _languageCode,
        zero: zero, one: one, two: two, few: few, many: many, other: other);

class Translations_ru extends Translations {
  const Translations_ru();
  Welcome_screenTranslations_ru get welcome_screen =>
      Welcome_screenTranslations_ru(this);
  Home_screenTranslations_ru get home_screen =>
      Home_screenTranslations_ru(this);
  Settings_screenTranslations_ru get settings_screen =>
      Settings_screenTranslations_ru(this);
  Under_constructionTranslations_ru get under_construction =>
      Under_constructionTranslations_ru(this);
  Object operator [](String key) {
    var index = key.indexOf('.');
    if (index > 0) {
      return (this[key.substring(0, index)]
          as i69n.I69nMessageBundle)[key.substring(index + 1)];
    }
    switch (key) {
      case 'welcome_screen':
        return welcome_screen;
      case 'home_screen':
        return home_screen;
      case 'settings_screen':
        return settings_screen;
      case 'under_construction':
        return under_construction;
      default:
        return super[key];
    }
  }
}

class Welcome_screenTranslations_ru extends Welcome_screenTranslations {
  final Translations_ru _parent;
  const Welcome_screenTranslations_ru(this._parent) : super(_parent);
  String get title => "Вступительная страница";
  String get settings => "Настройки";
  String get log_in_button => "Войти";
  String get sign_up_button => "Зарегестрироваться";
  Object operator [](String key) {
    var index = key.indexOf('.');
    if (index > 0) {
      return (this[key.substring(0, index)]
          as i69n.I69nMessageBundle)[key.substring(index + 1)];
    }
    switch (key) {
      case 'title':
        return title;
      case 'settings':
        return settings;
      case 'log_in_button':
        return log_in_button;
      case 'sign_up_button':
        return sign_up_button;
      default:
        return super[key];
    }
  }
}

class Home_screenTranslations_ru extends Home_screenTranslations {
  final Translations_ru _parent;
  const Home_screenTranslations_ru(this._parent) : super(_parent);
  String get title => "Домашняя Страница";
  String get log_out_button => "Выйти";
  Object operator [](String key) {
    var index = key.indexOf('.');
    if (index > 0) {
      return (this[key.substring(0, index)]
          as i69n.I69nMessageBundle)[key.substring(index + 1)];
    }
    switch (key) {
      case 'title':
        return title;
      case 'log_out_button':
        return log_out_button;
      default:
        return super[key];
    }
  }
}

class Settings_screenTranslations_ru extends Settings_screenTranslations {
  final Translations_ru _parent;
  const Settings_screenTranslations_ru(this._parent) : super(_parent);
  String get title => "Настройки";
  String get change_theme => "Выбор темы";
  String get change_language => "Выбор языка";
  Object operator [](String key) {
    var index = key.indexOf('.');
    if (index > 0) {
      return (this[key.substring(0, index)]
          as i69n.I69nMessageBundle)[key.substring(index + 1)];
    }
    switch (key) {
      case 'title':
        return title;
      case 'change_theme':
        return change_theme;
      case 'change_language':
        return change_language;
      default:
        return super[key];
    }
  }
}

class Under_constructionTranslations_ru extends Under_constructionTranslations {
  final Translations_ru _parent;
  const Under_constructionTranslations_ru(this._parent) : super(_parent);
  String get title => "В РАЗРАБОТКЕ";
  Object operator [](String key) {
    var index = key.indexOf('.');
    if (index > 0) {
      return (this[key.substring(0, index)]
          as i69n.I69nMessageBundle)[key.substring(index + 1)];
    }
    switch (key) {
      case 'title':
        return title;
      default:
        return super[key];
    }
  }
}
