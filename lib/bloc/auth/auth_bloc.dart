import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:kreios_app/router/app_router.gr.dart';
import 'package:kreios_app/service/repository/auth_repository.dart';

part 'auth_event.dart';
part 'auth_state.dart';
part 'auth_bloc.freezed.dart';

@injectable
class AuthBloc extends Bloc<AuthEvent, AuthState> {
  AuthBloc({AppRouter router, AuthRepository authRepository})
      : _authRepository = authRepository,
        _router = router,
        super(const AuthState());

  final AppRouter _router;
  final AuthRepository _authRepository;

  @override
  Stream<AuthState> mapEventToState(
    AuthEvent event,
  ) async* {
    yield* event.when(
      checkToken: _checkToken,
      updateRootScreen: _updateRootScreen,
    );
  }

  Stream<AuthState> _checkToken() async* {
    final token = await _authRepository.getToken();
    yield state.copyWith(token: token);
    add(const AuthEvent.updateRootScreen());
  }

  Stream<AuthState> _updateRootScreen() async* {
    await state.token.fold(
      () => _router.replace(const WelcomeScreenRoute()),
      (_) => _router.replace(const HomeScreenRoute()),
    );
  }
}
