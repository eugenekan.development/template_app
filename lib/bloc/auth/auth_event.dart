part of 'auth_bloc.dart';

@freezed
abstract class AuthEvent with _$AuthEvent {
  const factory AuthEvent.updateRootScreen() = _UpdateRootScreen;
  const factory AuthEvent.checkToken() = _CheckToken;
}
