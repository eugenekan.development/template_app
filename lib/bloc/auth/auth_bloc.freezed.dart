// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'auth_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$AuthEventTearOff {
  const _$AuthEventTearOff();

// ignore: unused_element
  _UpdateRootScreen updateRootScreen() {
    return const _UpdateRootScreen();
  }

// ignore: unused_element
  _CheckToken checkToken() {
    return const _CheckToken();
  }
}

/// @nodoc
// ignore: unused_element
const $AuthEvent = _$AuthEventTearOff();

/// @nodoc
mixin _$AuthEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult updateRootScreen(),
    @required TResult checkToken(),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult updateRootScreen(),
    TResult checkToken(),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult updateRootScreen(_UpdateRootScreen value),
    @required TResult checkToken(_CheckToken value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult updateRootScreen(_UpdateRootScreen value),
    TResult checkToken(_CheckToken value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $AuthEventCopyWith<$Res> {
  factory $AuthEventCopyWith(AuthEvent value, $Res Function(AuthEvent) then) =
      _$AuthEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$AuthEventCopyWithImpl<$Res> implements $AuthEventCopyWith<$Res> {
  _$AuthEventCopyWithImpl(this._value, this._then);

  final AuthEvent _value;
  // ignore: unused_field
  final $Res Function(AuthEvent) _then;
}

/// @nodoc
abstract class _$UpdateRootScreenCopyWith<$Res> {
  factory _$UpdateRootScreenCopyWith(
          _UpdateRootScreen value, $Res Function(_UpdateRootScreen) then) =
      __$UpdateRootScreenCopyWithImpl<$Res>;
}

/// @nodoc
class __$UpdateRootScreenCopyWithImpl<$Res>
    extends _$AuthEventCopyWithImpl<$Res>
    implements _$UpdateRootScreenCopyWith<$Res> {
  __$UpdateRootScreenCopyWithImpl(
      _UpdateRootScreen _value, $Res Function(_UpdateRootScreen) _then)
      : super(_value, (v) => _then(v as _UpdateRootScreen));

  @override
  _UpdateRootScreen get _value => super._value as _UpdateRootScreen;
}

/// @nodoc
class _$_UpdateRootScreen implements _UpdateRootScreen {
  const _$_UpdateRootScreen();

  @override
  String toString() {
    return 'AuthEvent.updateRootScreen()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _UpdateRootScreen);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult updateRootScreen(),
    @required TResult checkToken(),
  }) {
    assert(updateRootScreen != null);
    assert(checkToken != null);
    return updateRootScreen();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult updateRootScreen(),
    TResult checkToken(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (updateRootScreen != null) {
      return updateRootScreen();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult updateRootScreen(_UpdateRootScreen value),
    @required TResult checkToken(_CheckToken value),
  }) {
    assert(updateRootScreen != null);
    assert(checkToken != null);
    return updateRootScreen(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult updateRootScreen(_UpdateRootScreen value),
    TResult checkToken(_CheckToken value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (updateRootScreen != null) {
      return updateRootScreen(this);
    }
    return orElse();
  }
}

abstract class _UpdateRootScreen implements AuthEvent {
  const factory _UpdateRootScreen() = _$_UpdateRootScreen;
}

/// @nodoc
abstract class _$CheckTokenCopyWith<$Res> {
  factory _$CheckTokenCopyWith(
          _CheckToken value, $Res Function(_CheckToken) then) =
      __$CheckTokenCopyWithImpl<$Res>;
}

/// @nodoc
class __$CheckTokenCopyWithImpl<$Res> extends _$AuthEventCopyWithImpl<$Res>
    implements _$CheckTokenCopyWith<$Res> {
  __$CheckTokenCopyWithImpl(
      _CheckToken _value, $Res Function(_CheckToken) _then)
      : super(_value, (v) => _then(v as _CheckToken));

  @override
  _CheckToken get _value => super._value as _CheckToken;
}

/// @nodoc
class _$_CheckToken implements _CheckToken {
  const _$_CheckToken();

  @override
  String toString() {
    return 'AuthEvent.checkToken()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _CheckToken);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult updateRootScreen(),
    @required TResult checkToken(),
  }) {
    assert(updateRootScreen != null);
    assert(checkToken != null);
    return checkToken();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult updateRootScreen(),
    TResult checkToken(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (checkToken != null) {
      return checkToken();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult updateRootScreen(_UpdateRootScreen value),
    @required TResult checkToken(_CheckToken value),
  }) {
    assert(updateRootScreen != null);
    assert(checkToken != null);
    return checkToken(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult updateRootScreen(_UpdateRootScreen value),
    TResult checkToken(_CheckToken value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (checkToken != null) {
      return checkToken(this);
    }
    return orElse();
  }
}

abstract class _CheckToken implements AuthEvent {
  const factory _CheckToken() = _$_CheckToken;
}

/// @nodoc
class _$AuthStateTearOff {
  const _$AuthStateTearOff();

// ignore: unused_element
  _State call({Option<String> token = const None<String>()}) {
    return _State(
      token: token,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $AuthState = _$AuthStateTearOff();

/// @nodoc
mixin _$AuthState {
  Option<String> get token;

  @JsonKey(ignore: true)
  $AuthStateCopyWith<AuthState> get copyWith;
}

/// @nodoc
abstract class $AuthStateCopyWith<$Res> {
  factory $AuthStateCopyWith(AuthState value, $Res Function(AuthState) then) =
      _$AuthStateCopyWithImpl<$Res>;
  $Res call({Option<String> token});
}

/// @nodoc
class _$AuthStateCopyWithImpl<$Res> implements $AuthStateCopyWith<$Res> {
  _$AuthStateCopyWithImpl(this._value, this._then);

  final AuthState _value;
  // ignore: unused_field
  final $Res Function(AuthState) _then;

  @override
  $Res call({
    Object token = freezed,
  }) {
    return _then(_value.copyWith(
      token: token == freezed ? _value.token : token as Option<String>,
    ));
  }
}

/// @nodoc
abstract class _$StateCopyWith<$Res> implements $AuthStateCopyWith<$Res> {
  factory _$StateCopyWith(_State value, $Res Function(_State) then) =
      __$StateCopyWithImpl<$Res>;
  @override
  $Res call({Option<String> token});
}

/// @nodoc
class __$StateCopyWithImpl<$Res> extends _$AuthStateCopyWithImpl<$Res>
    implements _$StateCopyWith<$Res> {
  __$StateCopyWithImpl(_State _value, $Res Function(_State) _then)
      : super(_value, (v) => _then(v as _State));

  @override
  _State get _value => super._value as _State;

  @override
  $Res call({
    Object token = freezed,
  }) {
    return _then(_State(
      token: token == freezed ? _value.token : token as Option<String>,
    ));
  }
}

/// @nodoc
class _$_State implements _State {
  const _$_State({this.token = const None<String>()}) : assert(token != null);

  @JsonKey(defaultValue: const None<String>())
  @override
  final Option<String> token;

  @override
  String toString() {
    return 'AuthState(token: $token)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _State &&
            (identical(other.token, token) ||
                const DeepCollectionEquality().equals(other.token, token)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(token);

  @JsonKey(ignore: true)
  @override
  _$StateCopyWith<_State> get copyWith =>
      __$StateCopyWithImpl<_State>(this, _$identity);
}

abstract class _State implements AuthState {
  const factory _State({Option<String> token}) = _$_State;

  @override
  Option<String> get token;
  @override
  @JsonKey(ignore: true)
  _$StateCopyWith<_State> get copyWith;
}
