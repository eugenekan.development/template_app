part of 'auth_bloc.dart';

@freezed
abstract class AuthState with _$AuthState {
  const factory AuthState({
    @Default(None<String>()) Option<String> token,
  }) = _State;
}
