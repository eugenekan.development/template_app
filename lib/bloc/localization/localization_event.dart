part of 'localization_bloc.dart';

@freezed
abstract class LocalizationEvent with _$LocalizationEvent {
  const factory LocalizationEvent.setRuLanguage() = _SetRuLanguage;
  const factory LocalizationEvent.setEngLanguage() = _SetEngLanguage;
  const factory LocalizationEvent.changeLanguage(Locale locale) =
      _ChangeLanguage;
  const factory LocalizationEvent.load() = _LoadLanguage;
}
