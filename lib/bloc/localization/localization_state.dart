part of 'localization_bloc.dart';

@freezed
abstract class LocalizationState with _$LocalizationState {
  const factory LocalizationState({
    Locale locale,
  }) = LocalizationChangedLanguageState;
}
