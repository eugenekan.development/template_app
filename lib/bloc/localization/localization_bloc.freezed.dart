// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'localization_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$LocalizationEventTearOff {
  const _$LocalizationEventTearOff();

// ignore: unused_element
  _SetRuLanguage setRuLanguage() {
    return const _SetRuLanguage();
  }

// ignore: unused_element
  _SetEngLanguage setEngLanguage() {
    return const _SetEngLanguage();
  }

// ignore: unused_element
  _ChangeLanguage changeLanguage(Locale locale) {
    return _ChangeLanguage(
      locale,
    );
  }

// ignore: unused_element
  _LoadLanguage load() {
    return const _LoadLanguage();
  }
}

/// @nodoc
// ignore: unused_element
const $LocalizationEvent = _$LocalizationEventTearOff();

/// @nodoc
mixin _$LocalizationEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult setRuLanguage(),
    @required TResult setEngLanguage(),
    @required TResult changeLanguage(Locale locale),
    @required TResult load(),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult setRuLanguage(),
    TResult setEngLanguage(),
    TResult changeLanguage(Locale locale),
    TResult load(),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult setRuLanguage(_SetRuLanguage value),
    @required TResult setEngLanguage(_SetEngLanguage value),
    @required TResult changeLanguage(_ChangeLanguage value),
    @required TResult load(_LoadLanguage value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult setRuLanguage(_SetRuLanguage value),
    TResult setEngLanguage(_SetEngLanguage value),
    TResult changeLanguage(_ChangeLanguage value),
    TResult load(_LoadLanguage value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $LocalizationEventCopyWith<$Res> {
  factory $LocalizationEventCopyWith(
          LocalizationEvent value, $Res Function(LocalizationEvent) then) =
      _$LocalizationEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$LocalizationEventCopyWithImpl<$Res>
    implements $LocalizationEventCopyWith<$Res> {
  _$LocalizationEventCopyWithImpl(this._value, this._then);

  final LocalizationEvent _value;
  // ignore: unused_field
  final $Res Function(LocalizationEvent) _then;
}

/// @nodoc
abstract class _$SetRuLanguageCopyWith<$Res> {
  factory _$SetRuLanguageCopyWith(
          _SetRuLanguage value, $Res Function(_SetRuLanguage) then) =
      __$SetRuLanguageCopyWithImpl<$Res>;
}

/// @nodoc
class __$SetRuLanguageCopyWithImpl<$Res>
    extends _$LocalizationEventCopyWithImpl<$Res>
    implements _$SetRuLanguageCopyWith<$Res> {
  __$SetRuLanguageCopyWithImpl(
      _SetRuLanguage _value, $Res Function(_SetRuLanguage) _then)
      : super(_value, (v) => _then(v as _SetRuLanguage));

  @override
  _SetRuLanguage get _value => super._value as _SetRuLanguage;
}

/// @nodoc
class _$_SetRuLanguage implements _SetRuLanguage {
  const _$_SetRuLanguage();

  @override
  String toString() {
    return 'LocalizationEvent.setRuLanguage()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _SetRuLanguage);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult setRuLanguage(),
    @required TResult setEngLanguage(),
    @required TResult changeLanguage(Locale locale),
    @required TResult load(),
  }) {
    assert(setRuLanguage != null);
    assert(setEngLanguage != null);
    assert(changeLanguage != null);
    assert(load != null);
    return setRuLanguage();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult setRuLanguage(),
    TResult setEngLanguage(),
    TResult changeLanguage(Locale locale),
    TResult load(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (setRuLanguage != null) {
      return setRuLanguage();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult setRuLanguage(_SetRuLanguage value),
    @required TResult setEngLanguage(_SetEngLanguage value),
    @required TResult changeLanguage(_ChangeLanguage value),
    @required TResult load(_LoadLanguage value),
  }) {
    assert(setRuLanguage != null);
    assert(setEngLanguage != null);
    assert(changeLanguage != null);
    assert(load != null);
    return setRuLanguage(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult setRuLanguage(_SetRuLanguage value),
    TResult setEngLanguage(_SetEngLanguage value),
    TResult changeLanguage(_ChangeLanguage value),
    TResult load(_LoadLanguage value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (setRuLanguage != null) {
      return setRuLanguage(this);
    }
    return orElse();
  }
}

abstract class _SetRuLanguage implements LocalizationEvent {
  const factory _SetRuLanguage() = _$_SetRuLanguage;
}

/// @nodoc
abstract class _$SetEngLanguageCopyWith<$Res> {
  factory _$SetEngLanguageCopyWith(
          _SetEngLanguage value, $Res Function(_SetEngLanguage) then) =
      __$SetEngLanguageCopyWithImpl<$Res>;
}

/// @nodoc
class __$SetEngLanguageCopyWithImpl<$Res>
    extends _$LocalizationEventCopyWithImpl<$Res>
    implements _$SetEngLanguageCopyWith<$Res> {
  __$SetEngLanguageCopyWithImpl(
      _SetEngLanguage _value, $Res Function(_SetEngLanguage) _then)
      : super(_value, (v) => _then(v as _SetEngLanguage));

  @override
  _SetEngLanguage get _value => super._value as _SetEngLanguage;
}

/// @nodoc
class _$_SetEngLanguage implements _SetEngLanguage {
  const _$_SetEngLanguage();

  @override
  String toString() {
    return 'LocalizationEvent.setEngLanguage()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _SetEngLanguage);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult setRuLanguage(),
    @required TResult setEngLanguage(),
    @required TResult changeLanguage(Locale locale),
    @required TResult load(),
  }) {
    assert(setRuLanguage != null);
    assert(setEngLanguage != null);
    assert(changeLanguage != null);
    assert(load != null);
    return setEngLanguage();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult setRuLanguage(),
    TResult setEngLanguage(),
    TResult changeLanguage(Locale locale),
    TResult load(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (setEngLanguage != null) {
      return setEngLanguage();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult setRuLanguage(_SetRuLanguage value),
    @required TResult setEngLanguage(_SetEngLanguage value),
    @required TResult changeLanguage(_ChangeLanguage value),
    @required TResult load(_LoadLanguage value),
  }) {
    assert(setRuLanguage != null);
    assert(setEngLanguage != null);
    assert(changeLanguage != null);
    assert(load != null);
    return setEngLanguage(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult setRuLanguage(_SetRuLanguage value),
    TResult setEngLanguage(_SetEngLanguage value),
    TResult changeLanguage(_ChangeLanguage value),
    TResult load(_LoadLanguage value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (setEngLanguage != null) {
      return setEngLanguage(this);
    }
    return orElse();
  }
}

abstract class _SetEngLanguage implements LocalizationEvent {
  const factory _SetEngLanguage() = _$_SetEngLanguage;
}

/// @nodoc
abstract class _$ChangeLanguageCopyWith<$Res> {
  factory _$ChangeLanguageCopyWith(
          _ChangeLanguage value, $Res Function(_ChangeLanguage) then) =
      __$ChangeLanguageCopyWithImpl<$Res>;
  $Res call({Locale locale});
}

/// @nodoc
class __$ChangeLanguageCopyWithImpl<$Res>
    extends _$LocalizationEventCopyWithImpl<$Res>
    implements _$ChangeLanguageCopyWith<$Res> {
  __$ChangeLanguageCopyWithImpl(
      _ChangeLanguage _value, $Res Function(_ChangeLanguage) _then)
      : super(_value, (v) => _then(v as _ChangeLanguage));

  @override
  _ChangeLanguage get _value => super._value as _ChangeLanguage;

  @override
  $Res call({
    Object locale = freezed,
  }) {
    return _then(_ChangeLanguage(
      locale == freezed ? _value.locale : locale as Locale,
    ));
  }
}

/// @nodoc
class _$_ChangeLanguage implements _ChangeLanguage {
  const _$_ChangeLanguage(this.locale) : assert(locale != null);

  @override
  final Locale locale;

  @override
  String toString() {
    return 'LocalizationEvent.changeLanguage(locale: $locale)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _ChangeLanguage &&
            (identical(other.locale, locale) ||
                const DeepCollectionEquality().equals(other.locale, locale)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(locale);

  @JsonKey(ignore: true)
  @override
  _$ChangeLanguageCopyWith<_ChangeLanguage> get copyWith =>
      __$ChangeLanguageCopyWithImpl<_ChangeLanguage>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult setRuLanguage(),
    @required TResult setEngLanguage(),
    @required TResult changeLanguage(Locale locale),
    @required TResult load(),
  }) {
    assert(setRuLanguage != null);
    assert(setEngLanguage != null);
    assert(changeLanguage != null);
    assert(load != null);
    return changeLanguage(locale);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult setRuLanguage(),
    TResult setEngLanguage(),
    TResult changeLanguage(Locale locale),
    TResult load(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (changeLanguage != null) {
      return changeLanguage(locale);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult setRuLanguage(_SetRuLanguage value),
    @required TResult setEngLanguage(_SetEngLanguage value),
    @required TResult changeLanguage(_ChangeLanguage value),
    @required TResult load(_LoadLanguage value),
  }) {
    assert(setRuLanguage != null);
    assert(setEngLanguage != null);
    assert(changeLanguage != null);
    assert(load != null);
    return changeLanguage(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult setRuLanguage(_SetRuLanguage value),
    TResult setEngLanguage(_SetEngLanguage value),
    TResult changeLanguage(_ChangeLanguage value),
    TResult load(_LoadLanguage value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (changeLanguage != null) {
      return changeLanguage(this);
    }
    return orElse();
  }
}

abstract class _ChangeLanguage implements LocalizationEvent {
  const factory _ChangeLanguage(Locale locale) = _$_ChangeLanguage;

  Locale get locale;
  @JsonKey(ignore: true)
  _$ChangeLanguageCopyWith<_ChangeLanguage> get copyWith;
}

/// @nodoc
abstract class _$LoadLanguageCopyWith<$Res> {
  factory _$LoadLanguageCopyWith(
          _LoadLanguage value, $Res Function(_LoadLanguage) then) =
      __$LoadLanguageCopyWithImpl<$Res>;
}

/// @nodoc
class __$LoadLanguageCopyWithImpl<$Res>
    extends _$LocalizationEventCopyWithImpl<$Res>
    implements _$LoadLanguageCopyWith<$Res> {
  __$LoadLanguageCopyWithImpl(
      _LoadLanguage _value, $Res Function(_LoadLanguage) _then)
      : super(_value, (v) => _then(v as _LoadLanguage));

  @override
  _LoadLanguage get _value => super._value as _LoadLanguage;
}

/// @nodoc
class _$_LoadLanguage implements _LoadLanguage {
  const _$_LoadLanguage();

  @override
  String toString() {
    return 'LocalizationEvent.load()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _LoadLanguage);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult setRuLanguage(),
    @required TResult setEngLanguage(),
    @required TResult changeLanguage(Locale locale),
    @required TResult load(),
  }) {
    assert(setRuLanguage != null);
    assert(setEngLanguage != null);
    assert(changeLanguage != null);
    assert(load != null);
    return load();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult setRuLanguage(),
    TResult setEngLanguage(),
    TResult changeLanguage(Locale locale),
    TResult load(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (load != null) {
      return load();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult setRuLanguage(_SetRuLanguage value),
    @required TResult setEngLanguage(_SetEngLanguage value),
    @required TResult changeLanguage(_ChangeLanguage value),
    @required TResult load(_LoadLanguage value),
  }) {
    assert(setRuLanguage != null);
    assert(setEngLanguage != null);
    assert(changeLanguage != null);
    assert(load != null);
    return load(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult setRuLanguage(_SetRuLanguage value),
    TResult setEngLanguage(_SetEngLanguage value),
    TResult changeLanguage(_ChangeLanguage value),
    TResult load(_LoadLanguage value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (load != null) {
      return load(this);
    }
    return orElse();
  }
}

abstract class _LoadLanguage implements LocalizationEvent {
  const factory _LoadLanguage() = _$_LoadLanguage;
}

/// @nodoc
class _$LocalizationStateTearOff {
  const _$LocalizationStateTearOff();

// ignore: unused_element
  LocalizationChangedLanguageState call({Locale locale}) {
    return LocalizationChangedLanguageState(
      locale: locale,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $LocalizationState = _$LocalizationStateTearOff();

/// @nodoc
mixin _$LocalizationState {
  Locale get locale;

  @JsonKey(ignore: true)
  $LocalizationStateCopyWith<LocalizationState> get copyWith;
}

/// @nodoc
abstract class $LocalizationStateCopyWith<$Res> {
  factory $LocalizationStateCopyWith(
          LocalizationState value, $Res Function(LocalizationState) then) =
      _$LocalizationStateCopyWithImpl<$Res>;
  $Res call({Locale locale});
}

/// @nodoc
class _$LocalizationStateCopyWithImpl<$Res>
    implements $LocalizationStateCopyWith<$Res> {
  _$LocalizationStateCopyWithImpl(this._value, this._then);

  final LocalizationState _value;
  // ignore: unused_field
  final $Res Function(LocalizationState) _then;

  @override
  $Res call({
    Object locale = freezed,
  }) {
    return _then(_value.copyWith(
      locale: locale == freezed ? _value.locale : locale as Locale,
    ));
  }
}

/// @nodoc
abstract class $LocalizationChangedLanguageStateCopyWith<$Res>
    implements $LocalizationStateCopyWith<$Res> {
  factory $LocalizationChangedLanguageStateCopyWith(
          LocalizationChangedLanguageState value,
          $Res Function(LocalizationChangedLanguageState) then) =
      _$LocalizationChangedLanguageStateCopyWithImpl<$Res>;
  @override
  $Res call({Locale locale});
}

/// @nodoc
class _$LocalizationChangedLanguageStateCopyWithImpl<$Res>
    extends _$LocalizationStateCopyWithImpl<$Res>
    implements $LocalizationChangedLanguageStateCopyWith<$Res> {
  _$LocalizationChangedLanguageStateCopyWithImpl(
      LocalizationChangedLanguageState _value,
      $Res Function(LocalizationChangedLanguageState) _then)
      : super(_value, (v) => _then(v as LocalizationChangedLanguageState));

  @override
  LocalizationChangedLanguageState get _value =>
      super._value as LocalizationChangedLanguageState;

  @override
  $Res call({
    Object locale = freezed,
  }) {
    return _then(LocalizationChangedLanguageState(
      locale: locale == freezed ? _value.locale : locale as Locale,
    ));
  }
}

/// @nodoc
class _$LocalizationChangedLanguageState
    implements LocalizationChangedLanguageState {
  const _$LocalizationChangedLanguageState({this.locale});

  @override
  final Locale locale;

  @override
  String toString() {
    return 'LocalizationState(locale: $locale)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is LocalizationChangedLanguageState &&
            (identical(other.locale, locale) ||
                const DeepCollectionEquality().equals(other.locale, locale)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(locale);

  @JsonKey(ignore: true)
  @override
  $LocalizationChangedLanguageStateCopyWith<LocalizationChangedLanguageState>
      get copyWith => _$LocalizationChangedLanguageStateCopyWithImpl<
          LocalizationChangedLanguageState>(this, _$identity);
}

abstract class LocalizationChangedLanguageState implements LocalizationState {
  const factory LocalizationChangedLanguageState({Locale locale}) =
      _$LocalizationChangedLanguageState;

  @override
  Locale get locale;
  @override
  @JsonKey(ignore: true)
  $LocalizationChangedLanguageStateCopyWith<LocalizationChangedLanguageState>
      get copyWith;
}
