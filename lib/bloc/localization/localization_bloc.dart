import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secured_storage/flutter_secured_storage.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';

part 'localization_event.dart';
part 'localization_state.dart';
part 'localization_bloc.freezed.dart';

@injectable
class LocalizationBloc extends Bloc<LocalizationEvent, LocalizationState> {
  LocalizationBloc(this._flutterSecuredStorage)
      : super(const LocalizationChangedLanguageState()) {
    add(const LocalizationEvent.load());
  }
  final FlutterSecuredStorage _flutterSecuredStorage;

  @override
  Stream<LocalizationState> mapEventToState(
    LocalizationEvent event,
  ) async* {
    yield* event.when(
      setEngLanguage: _setEngLanguage,
      setRuLanguage: _setRuLanguage,
      load: _load,
      changeLanguage: _changeLanguage,
    );
  }

  Stream<LocalizationState> _load() async* {
    final language = await _flutterSecuredStorage.read(key: 'language');
    print('_load $language');

    if (language != null) {
      yield state.copyWith(
        locale: Locale(language),
      );
    } else {
      yield state.copyWith(
        locale: const Locale('en'),
      );
    }
  }

  Stream<LocalizationState> _changeLanguage(Locale locale) async* {
    print('_setRuLanguage');
    await _flutterSecuredStorage.write(
        key: 'language', value: locale.languageCode);

    yield state.copyWith(
      locale: locale,
    );
  }

  Stream<LocalizationState> _setRuLanguage() async* {
    print('_setRuLanguage');
  }

  Stream<LocalizationState> _setEngLanguage() async* {
    print('_setEngLanguage');
  }
}
