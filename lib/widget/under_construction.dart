import 'package:flutter/material.dart';
import 'package:kreios_app/localization/k_localization.dart';

class UnderConstruction extends StatelessWidget {
  const UnderConstruction({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Icon(Icons.construction),
              const SizedBox(height: 12),
              Text(KLocalization.of(context).under_construction.title),
            ],
          ),
        ),
      );
}
