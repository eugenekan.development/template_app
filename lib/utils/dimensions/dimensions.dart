abstract class DimensionsSchemes {
  DimensionsSchemes._();

  static const s = DimensionsScheme(
    xxs: 4.0,
    xs: 8.0,
    s: 12.0,
    m: 16.0,
    l: 24.0,
    xl: 32.0,
    xxl: 48.0,
  );

  static final xs = s * 0.8;
  static final m = s * 1.15;
  static final l = s * 1.3;
}

class DimensionsScheme {
  const DimensionsScheme({
    this.xxs,
    this.xs,
    this.s,
    this.m,
    this.l,
    this.xl,
    this.xxl,
  });

  final double xxs;
  final double xs;
  final double s;
  final double m;
  final double l;
  final double xl;
  final double xxl;

  DimensionsScheme operator *(double operand) => DimensionsScheme(
        xxs: xxs * operand,
        xs: xs * operand,
        s: s * operand,
        m: m * operand,
        l: l * operand,
        xl: xl * operand,
        xxl: xxl * operand,
      );
}
