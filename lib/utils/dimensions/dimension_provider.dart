import 'package:flutter/material.dart';
import 'package:kreios_app/utils/dimensions/dimensions.dart';

class DimensionsProvider extends InheritedWidget {
  const DimensionsProvider({
    @required this.dimensions,
    @required Widget child,
    Key key,
  })  : assert(child != null),
        super(key: key, child: child);

  DimensionsProvider.fromSize({
    @required Size size,
    @required Widget child,
    Key key,
  }) : this(
          key: key,
          dimensions: _getDimensionsScheme(size),
          child: child,
        );

  final DimensionsScheme dimensions;

  static DimensionsProvider of(BuildContext context) =>
      context.dependOnInheritedWidgetOfExactType<DimensionsProvider>();

  @override
  bool updateShouldNotify(DimensionsProvider oldWidget) =>
      dimensions.s != oldWidget.dimensions.s;

  static DimensionsScheme _getDimensionsScheme(Size size) {
    final square = size.width * size.height;

    if (square > 1000000) {
      // iPad Pro 12.9''
      return DimensionsSchemes.l;
    }
    if (square > 700000) {
      // iPad mini
      return DimensionsSchemes.m;
    }
    if (square > 300000) {
      // iPhone 11 Pro Max
      return DimensionsSchemes.s;
    }
    // iPhone SE
    return DimensionsSchemes.xs;
  }
}
