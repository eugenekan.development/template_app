import 'package:flutter/material.dart';

const Color featureBagColor = Color(0xFFF6F7FA);
const Color bagColor = Color(0xFFFFFFFF);

const Color fontLogoColor = Color(0xFF27233D);
